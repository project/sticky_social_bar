The sticky social bar module allows the user to display social share links on different pages with ablilty to share 
own site and other.

Sticky social share is rendered as block and you can customize as much
as you can in Configuration page and settings page.


Installation
------------
Standard module installation applies.


Configuration
-------------
Configuration page(admin/config/media/sticky-social-bar)
where you need to set variables .
