<?php
/**
 * @file
 * Contains \Drupal\sticky_social_bar\StickySocialBarCounter.
 */

namespace Drupal\sticky_social_bar;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Symfony\Component\HttpFoundation;
use Drupal\vendor\guzzlehttp;
use Drupal\Core\Http\ClientFactory;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use Drupal\Core\Render\HtmlResponseAttachmentsProcessor;
use GuzzleHttp\Psr7\Request;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManager;
use Drupal\Component\Utility;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;

/**
 * Sticky Social Bar Counter Manager Class.
 */
class StickySocialBarCounter
{

  public $node_id;

  protected $current_url;

  /**
   * The ID of this plugin.
   *
   * @since    1.0.6
   * @access   private
   * @var      string    $sb_bar    The ID of this plugin.
   */
  //private $sb_bar;

  /**
   * The version of this plugin.
   *
   * @since    1.0.6
   * @access   private
   * @var      string    $version    The current version of this plugin.
   */
  //private $version;

  /**
   * Plugin options
   *
   * @since    1.0.6
   * @access   private
   * @var      array    $options    Current plugin options
   */
  //private $options;

  /**
   * The name of the transient settings
   *
   * @since    1.0.6
   * @access   private
   * @var      string    $transient_name    The name of the transient settings
   */
  private $transient_name;

  function __construct($node_id)
  {
    global $base_url;

    $acturl = \Drupal::request()->getHost();
    $node = \Drupal::routeMatch()->getParameter('node');
    $node_id = $node->id(); // get current node id (current url node id)

    $current_path = \Drupal::service('path.current')->getPath();
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);

    $acturl = $base_url . $alias;
    $current_uri = \Drupal::request()->getRequestUri();

    $tempstoreurl = \Drupal::service('tempstore.private')->get('sticky_social_bar');
    $urlstorageval = $tempstoreurl->get('urlstoragevalue');

    $urltextbxval = $urlstorageval['0'];
    $urlchkboxval = $urlstorageval['1'];
    $test11 = encrypt_decrypt($urltextbxval, 'decrypt');

    $tempstore = \Drupal::service('tempstore.private')->get('sticky_social_bar');
    $urlchkboxendecry = $tempstore->get('my_variable_name');
    $pwd22 = encrypt_decrypt($urlchkboxendecry, 'decrypt');
    //for custom url from settings page
    if ($urlchkboxval == 1)
    {
      $this->current_url = $urltextbxval;
    }
    else
    {
      $this->current_url = $acturl;
    }

    $this->transient_name = $node_id . '_shares';
    $this->post_id = $node_id;
  }

  public function get_shares_all()
  {
    $activeNetworks = array();
    $networks = array(
      'stumbleupon',
      'pinterest'
    );

    //Filter disabled networks
    foreach ($networks as $network)
    {
      $activeNetworks[] = $network;
    }

    $shares = array_fill_keys(array_values($networks) , 0);

    if (empty($shares))
    {
      foreach ($activeNetworks as $network)
      {
        $shares[$network] = $this->{'get_shares_' . $network}($this->post_id);
        $shares[$network] = $this->share_count_pretty($shares[$network]);
      }
    }

    return $shares;

  }

  public function get_shares_stumbleupon($node_id)
  {
    $protocol = 'https';
    $client = \Drupal::httpClient();

    $stumbleupon = $client->request('GET', (strtolower($protocol) == 'http' ? 'http' : 'https') . '://www.stumbleupon.com/services/1.01/badge.getinfo?url=' . $this->current_url);

    if ($stumbleupon->getStatusCode() == 200)
    {
      $body = $stumbleupon->getBody(true);

      if (($stumbleupon))
      {
        $response = $stumbleupon->getBody(true);
        $encoded_response = json_decode($response, true);  
        $stumbleuponshare_count = intval($encoded_response['result']['views']);
      }
      else
      {
        $stumbleuponshare_count = 0;
      }

    }

    return $stumbleuponshare_count;
  }

  public function get_shares_facebook($node_id)
  {
    $protocol = 'https';
    $tempstorefb = \Drupal::service('tempstore.private')->get('sticky_social_bar');
    $fbstorageval = $tempstorefb->get('fbstoragevalue');
    $apisecretid = $fbstorageval['0'];
    $apipagename = $fbstorageval['1'];
    $apiclieid = $fbstorageval['2'];
    $apisecret = encrypt_decrypt($apisecretid, 'decrypt');
    $apipagenm = encrypt_decrypt($apipagename, 'decrypt');
    $apiclientid = encrypt_decrypt($apiclieid, 'decrypt');

    $client = \Drupal::httpClient();

    $facebook_app_id = $apiclientid;
    $facebook_app_secret = $apisecret;
    $access_token = $facebook_app_id . '|' . $facebook_app_secret;
    $response = $client->request('GET', (strtolower($protocol) == 'http' ? 'http' : 'https') . '://graph.facebook.com/?fields=engagement&id=' . $this->current_url . '&access_token=' . $access_token, array(
      'timeout' => 20
    ));

    if ($response->getStatusCode() == 200)
    {
      $body = $response->getBody(true);
      if (($response))
      {
        $parse_response = json_decode($body, true);
        $fb_share_count = $parse_response['data'][0]['share_count'];

      }
      else
      {
        $fb_share_count = 0;
      }
    }

    return $response;
  }

  public function get_shares_reddit($node_id)
  {
    $protocol = 'https';

    $client = \Drupal::httpClient();

    $reddit = $client->request('GET', (strtolower($protocol) == 'http' ? 'http' : 'https') . '://www.reddit.com/api/info.json?url=' . $this->current_url);

    if ($reddit->getStatusCode() == 200)
    {
      $body = $reddit->getBody(true);

      if (($reddit))
      {
        $reddit = json_decode($body, true);
        $reddit = $reddit['count'] == NULL ? 0 : $reddit['count'];
      }
      else
      {
        $reddit = 0;
      }

    }
  }

  public function get_shares_pinterest($node_id)
  {
    $protocol = 'https';
    $client = \Drupal::httpClient();  
    try
    {
      $response = $client->request('GET', (strtolower($protocol) == 'http' ? 'http' : 'https') . '://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url=' . $this->current_url);
      
      if ($response->getStatusCode() == 200)
      {
        $body = $response->getBody(true);
        if (($response))
        {
          $response = json_decode(preg_replace('/^receiveCount\((.*)\)$/', "\\1", $body) , true);
          $response = $response['count'] == NULL ? 0 : $response['count'];
        }
        else
        {
          $response = 0;
        }
      }
    }
    catch(RequestException $e)
    {       
      watchdog_exception('sticky_social_bar', $e->getMessage());
    }

    return $response;

  }

  /**
   * Format the share count number to eg. 1k, 2.2k etc.
   *
   * @since    1.0.6
   */
  public function share_count_pretty($ugly_count)
  {

    $count = (int)$ugly_count;

    if ($count < 1000)
    {
      return $count;
    }
    else if ($count < 9999)
    {
      $count = round($count / 1000, 1) . 'k';
    }
    else if ($count < 99999)
    {
      $count = round($count / 1000, 0) . 'k';
    }
    else
    {
      $count = round($count / 1000000, 1) . 'm';
    }

    return $count;

  }

}
?>
