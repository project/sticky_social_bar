<?php
namespace Drupal\sticky_social_bar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Cache\Cache;
use Drupal\user\Entity;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\sticky_social_bar\StickySocialBarCounter;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'SocialStickyBarBlock' block.
 *
 * @Block(
 *  id = "social_sticky_block",
 *  admin_label = @Translation("Social Sticky Bar block"),
 * )
 */
class SocialStickyBarBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * The configFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, Token $token, CurrentPathStack $current_path)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->token = $token;
    //$this->eventDispatcher = $event_dispatcher;
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static ($configuration, $plugin_id, $plugin_definition, $container->get('config.factory') , $container->get('token') , $container->get('path.current'));
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    global $base_url;
    $valapiurl = [];
    $currcomment = '';
    $currurlblock = '';
    $currurlblockdis = '';
    $currbodytrim = '';
    $icon_path = $base_url . '/' . drupal_get_path('module', 'sticky_social_bar') . '/icons/';
    $sticky_social_bar = $this
      ->configFactory
      ->get('sticky_social_bar.settings')
      ->get('sticky_social_bar');
    $display_role = \Drupal::currentUser()->getRoles();

    $roledis = $display_role['0'];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface)
    {
      $nid = $node->id();
      $nidbundle = $node->bundle();
      $node->toLink()->toString();
      $currtitle = isset($node->get('title')->value) ? $node->get('title')->value : '';
      $author_id = $node->getOwner()->id();
      $account = \Drupal\user\Entity\User::load($author_id); // pass your uid
      $currname = is_null($account->getDisplayName()) ? $account->getDisplayName() : '';

      if ($node->hasField('field_image'))
      {
        //get first image on multi value field
        if (!empty($node->field_image[0]))
        {
          $image = $node->field_image[0]->getValue();
        }

        $media_field = $node->get('field_image')->getString();
        $fid = $node->get('field_image')->target_id;
        $currimage_file = File::load($fid);
        $curruri = $currimage_file->uri->value;
        $currurlblock = file_create_url($curruri);
      }
      if ($node->hasField('field_displayimg'))
      {
        //get first image on multi value field
        if (!empty($node->field_displayimg[0]))
        {
          $imagedis = $node->field_displayimg[0]->getValue();
        }

        $media_fielddis = $node->get('field_displayimg')->getString(); // Get media ID from your field.
        $fiddis = $node->get('field_displayimg')->target_id;
        $currimage_filedis = File::load($fiddis);
        $curruridis = $currimage_filedis->uri->value;
        $currurlblockdis = file_create_url($curruridis);
      }
      if ($node->hasField('body'))
      {
        $currbody = $node->get('body')->value;
        $currsumm = $node->get('body')->summary;
        $currbodytrim = substr($currbody, 0, 25);        
      }

      if ($node->hasField('comment'))
      {
        $currcomment = $node->get('comment')->comment_count;

      }

    }

    $dispintchkbox = $sticky_social_bar['enable_share_pinterest'];
    $dislinkchkbox = $sticky_social_bar['enable_share_linkedin'];
    $distweetchkbox = $sticky_social_bar['enable_share_twitter'];
    $disinstachkbox = $sticky_social_bar['enable_share_instagram'];
    $disredditchkbox = $sticky_social_bar['enable_share_reddit'];
    $disstumbleuponchkbox = $sticky_social_bar['enable_share_stumbleupon'];
    $diswhatsappchkbox = $sticky_social_bar['enable_whatsapp'];
    $disfbookchkbox = $sticky_social_bar['enable_share_fbook'];
    $disemailchkbox = $sticky_social_bar['disable_email'];
    $disauthorchkbox = $sticky_social_bar['enable_author'];
    $enable_sharechkbox = $sticky_social_bar['enable_share'];
    $disbarchkbox = $sticky_social_bar['enable_bar'];
    $dissharecountchkbox = $sticky_social_bar['enable_share_count'];
    $cuscolor = $sticky_social_bar['custom_color'];
    $frm_node_types = $sticky_social_bar['node_types'];
    $urlchk = isset($sticky_social_bar['urlchk']) ? $sticky_social_bar['urlchk'] : '';
    $urlupdate_check = $sharepinterest = isset($sticky_social_bar['urlupdate_check']) ? $sticky_social_bar['urlupdate_check'] : '';
    $role_permissions = isset($sticky_social_bar['role_permissions']) ? $sticky_social_bar['role_permissions'] : '';

    foreach ($sticky_social_bar as $name => $social_media)
    {
      if ($disbarchkbox == 1 && !empty($social_media['api_url']))
      {
        $valapiurl[$name]['api'] = $this
          ->token
          ->replace($social_media['api_url']);
      }
    }

    $nodecurren = \Drupal::routeMatch()->getParameter('node');
    if (!empty($nodecurren) || isset($nodecurren))
    {
      $nodecurr_id = $nodecurren->id();
      $social = new StickySocialBarCounter($nodecurr_id);
      $social_shares = $social->get_shares_all();
      $sharepinterest = isset($social_shares['pinterest']) ? $social_shares['pinterest'] : '';
      $sharestumbleupon = isset($social_shares['stumbleupon']) ? $social_shares['stumbleupon'] : '';
      $sharereddit = isset($social_shares['reddit']) ? $social_shares['reddit'] : '';
      $sharefb = isset($social_shares['facebook']) ? $social_shares['facebook'] : '';

    }
    $fbshareurl = $valapiurl['facebook_share']['api'];
    $instagram = $valapiurl['instagram']['api'];
    $linkedinurl = $valapiurl['linkedin']['api'];
    $tweeturl = $valapiurl['twitter']['api'];
    $pinteresturl = $valapiurl['pinterest']['api'];
    $whatsappurl = $valapiurl['whatsapp']['api'];
    $stumbleuponurl = $valapiurl['stumbleupon']['api'];
    $redditurl = isset($valapiurl['reddit']['api']) ? $valapiurl['reddit']['api'] : '';
    $emailurl = $valapiurl['email']['api'];
    return array(
      '#theme' => 'sticky_social_bar_display',
      '#cuscolor' => $cuscolor,
      '#frm_node_types' => $frm_node_types,
      '#currtitle' => isset($currtitle) ? $currtitle : '', //$currtitle,
      '#currname' => isset($currname) ? $currname : '',
      '#currbodytrim' => $currbodytrim,
      '#bundletype' => $nidbundle,
      '#currurlblock' => $currurlblock,
      '#currurlblockdis' => $currurlblockdis,
      '#currcomment' => $currcomment,
      '#chkbox_pinterest' => $dispintchkbox,
      '#chkbox_linkedin' => $dislinkchkbox,
      '#chkbox_twitter' => $distweetchkbox,
      '#chkbox_instagram' => $disinstachkbox,
      '#chkbox_reddit' => $disredditchkbox,
      '#chkbox_stumbleupon' => $disstumbleuponchkbox,
      '#chkbox_whatsapp' => $diswhatsappchkbox,
      '#chkbox_fbook' => $disfbookchkbox,
      '#disable_email' => $disemailchkbox,
      '#enable_author' => $disauthorchkbox,
      '#disbarchkbox' => $disbarchkbox,
      '#enable_sharechkbox' => $enable_sharechkbox,
      '#dissharecountchkbox' => $dissharecountchkbox,
      '#fbshareurl' => $fbshareurl,
      '#instagramurl' => $instagram,
      '#linkedinurl' => $linkedinurl,
      '#tweeturl' => $tweeturl,
      '#pinteresturl' => $pinteresturl,
      '#whatsappurl' => $whatsappurl,
      '#stumbleuponurl' => $stumbleuponurl,
      '#redditurl' => $redditurl,
      '#emailurl' => $emailurl,
      '#social_shares' => isset($social_shares) ? $social_shares : '',
      '#sharepinterest' => isset($sharepinterest) ? $sharepinterest : '',
      '#sharereddit' => isset($sharereddit) ? $sharereddit : '',
      '#sharestumbleupon' => isset($sharestumbleupon) ? $sharestumbleupon : '',
      '#sharefb' => isset($sharefb) ? $sharefb : '',
      '#urlchk' => $urlchk,
      '#urlupdate_check' => $urlupdate_check,
      '#role_permissions' => $role_permissions,
      '#roledis' => $roledis,
      '#display_role' => $display_role,
      'prev_display' => NULL,
      'prev_text' => NULL,
      'prev_id' => NULL,
      'next_display' => NULL,
      'next_text' => NULL,
      'next_id' => NULL,
      'nametest' => NULL,
      '#attached' => ['library' => ['sticky_social_bar/sticky_social_bar',
      ],
      ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags()
  {
    return Cache::mergeTags(parent::getCacheTags() , ['sticky_social_bar:' . $this
      ->currentPath
      ->getPath() , 'config:sticky_social_bar.settings', ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts()
  {
    return Cache::mergeContexts(parent::getCacheContexts() , ['url.path']);
  }

}

