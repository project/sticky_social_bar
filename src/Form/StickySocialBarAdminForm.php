<?php
namespace Drupal\sticky_social_bar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\TranslatableMarkup; 
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Link;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse; 
use Drupal\Core\Session\SessionManager;
use Drupal\Component\Utility;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\Core\Session\AccountInterface;

/**
 * Class StickySocialBarAdminForm.
 */
class StickySocialBarAdminForm extends ConfigFormBase
{
  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Constructs a StickySocialBarAdminForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The factory for configuration objects.
   */
  public function __construct(ModuleHandler $module_handler)
  {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static ($container->get('module_handler'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'sticky_social_bar_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['sticky_social_bar.settings', ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    global $base_path;

    $form = parent::buildForm($form, $form_state);
    $sticky_social_share = $this->getSocialSBar();
    $config = $this->config('sticky_social_bar.settings');
    $token_types = ['current_page'];

    $form['sticky_social_bar_button_settings'] = [
      '#type'         => 'details',
      '#title'        => $this->t('Enable Disable Settings'),
      '#open'         => TRUE,
    ];

    $form['sticky_social_bar_button_settings']['enable_author'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Author.'),
    '#default_value' => $config->get('sticky_social_bar.enable_author'),
    );

    $form['sticky_social_bar_button_settings']['enable_share'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Share'),
      '#default_value' => $config->get('sticky_social_bar.enable_share'),
    );
    $form['sticky_social_bar_button_settings']['enable_share_count'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Share Count'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_count'),
    );
    $form['sticky_social_bar_button_settings']['enable_share_pinterest'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Pinterest Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_pinterest'),
    );
    $form['sticky_social_bar_button_settings']['enable_share_linkedin'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable LinkedIn Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_linkedin'),
    );
    $form['sticky_social_bar_button_settings']['enable_share_twitter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Twitter Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_twitter'),
    );
    $form['sticky_social_bar_button_settings']['enable_share_instagram'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Instagram Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_instagram'),
    );


    $form['sticky_social_bar_button_settings']['enable_share_reddit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Reddit Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_reddit'),
    );

    $form['sticky_social_bar_button_settings']['enable_share_stumbleupon'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable stumbleupon Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_stumbleupon'),
    );
    $form['sticky_social_bar_button_settings']['enable_whatsapp'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Whatsapp Button'),
      '#default_value' => $config->get('sticky_social_bar.enable_whatsapp'),
    );

    $form['sticky_social_bar_button_settings']['enable_share_fbook'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Facebook Button (Why would you do that?)'),
      '#default_value' => $config->get('sticky_social_bar.enable_share_fbook'),
    );
     $form['sticky_social_bar_button_settings']['disable_email'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Email Button'),
      '#default_value' => $config->get('sticky_social_bar.disable_email'),
    );

    /* General settings from settings */
    $form['sticky_social_bar_additional_settings'] = [
      '#type'         => 'details',
      '#title'        => $this->t('General Settings'),
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
    ];
     $form['sticky_social_bar_additional_settings']['enable_bar'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Bar'),
      '#default_value' => $config->get('sticky_social_bar.enable_bar'),
    );

    $optionscolor  = array('blue' => t('Blue'),
                          'red' => t('Red'),
                          'green' => t('Green'),
                          'brown' => t('Brown'),
                          'orange' => t('Orange'),
                          'yellow' => t('Yellow'),
                          'purple' => t('Purple'));

    $form['sticky_social_bar_additional_settings']['custom_color'] = array(
      '#title' => t('Project Type'),
      '#type' => 'select',
      '#description' => "Select the project count type.",
      '#options' => $optionscolor,
      '#default_value' => $config->get('sticky_social_bar.custom_color'),
    );

    $roles = \Drupal::currentUser()->getRoles();

    $form['sticky_social_bar_additional_settings']['role_permissions'] = array(
      '#title' => t('Select the roles'),
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#default_value' => $config->get('sticky_social_bar.role_permissions'),
    );
    /* End General settings from settings */

    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();

    $node_type_titles = array();

    foreach ($node_types as $machine_name => $val)
    {
      $node_type_titles[$machine_name] = $val->label();
    }

    $form['sticky_social_bar_additional_settings']['node_types'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select Node Types'),
      '#options' => $node_type_titles,
      '#default_value' => $config->get('sticky_social_bar.node_types'),
    );

    /* Social share url settings from settings */
    $form['sticky_social_bar_url_settings'] = [
    '#type'         => 'details',
    '#title'        => $this->t('Share URL Settings'),
    '#open'         => FALSE,
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
    ];

    //For taking the url as same or other url for share link
    $form['sticky_social_bar_url_settings']['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('For taking the url as same or other url for share link'),
    ];

    $form['sticky_social_bar_url_settings']['urlupdate_check'] = [
      '#type' => 'checkbox',
      '#title' => 'Enter URL for share if you want to change it.',
      '#default_value' => $config->get('sticky_social_bar.urlupdate_check'),
    ];


    $form['sticky_social_bar_url_settings']['urlupdatecheck'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'urlupdatecheck',
      ],

      '#states' => [
        'invisible' => [
          ':input[name="urlupdate_check"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['sticky_social_bar_url_settings']['urlchk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Share'),
      '#default_value' => $config->get('sticky_social_bar.urlchk'),
      '#description' => $this->t('External URL Share like http://www.google.com or https://www.google.com.'),
    ];
    // end share url

    foreach ($sticky_social_share as $key => $label) {
      $form['sticky_social_bar_url_settings'][$key] = [
        '#type' => 'details',
        '#title' => t('@sticky_social_bar settings', ['@sticky_social_bar' => $label]),
        '#open' => TRUE,
      ];


     $form['sticky_social_bar_url_settings'][$key][$key . '_api_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t(strtoupper($label) .' API url'),
      '#default_value' => $config->get('sticky_social_bar.' . $key . '.api_url'),
      '#description' => t(strtoupper($label) . ' API URL'),
    ];

    // Add the token tree UI.
    $form['sticky_social_bar_url_settings'][$key]['token_frm'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' =>  $token_types,
      '#click_insert' => TRUE,
      '#dialog' => TRUE,
      '#global_types' => TRUE,
      '#weight' => 90,
    );

      if($key == 'facebook_share') {
      $form['sticky_social_bar_url_settings'][$key][$key . '_api_accesstoken_app_id'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Facebook App ID'),
        '#default_value' => $config->get('sticky_social_bar.' . $key . '.api_accesstoken_app_id'),
        '#description' => t(strtoupper($label) . ' API URL'),
      ];

       $form['sticky_social_bar_url_settings'][$key][$key . '_api_accesstoken_app_secret'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Facebook API Secret'),
        '#default_value' => $config->get('sticky_social_bar.' . $key . '.api_accesstoken_app_secret'),
        '#description' => t(strtoupper($label) . ' API URL'),
      ];

       $form['sticky_social_bar_url_settings'][$key][$key . '_api_accesstoken_pagename_app'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Facebook API Page Name'),
        '#default_value' => $config->get('sticky_social_bar.' . $key . '.api_accesstoken_pagename_app'),
        '#description' => t(strtoupper($label) . ' API URL'),
      ];
     }
    }

    /* end Social share url settings from settings */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {   
    $socialfrmsub = $this->getSocialSBar();
    $values = $form_state->getValues();

    $keycolor = $form_state->getValue('custom_color');

    $config = $this->config('sticky_social_bar.settings');

    $config->set('sticky_social_bar.enable_author', $values['enable_author']);   
    $config->set('sticky_social_bar.enable_share', $values['enable_share']);
    $config->set('sticky_social_bar.enable_share_count', $values['enable_share_count']);
    $config->set('sticky_social_bar.enable_share_pinterest', $values['enable_share_pinterest']);
    $config->set('sticky_social_bar.enable_share_linkedin', $values['enable_share_linkedin']);
    $config->set('sticky_social_bar.enable_share_twitter', $values['enable_share_twitter']);
    $config->set('sticky_social_bar.enable_share_instagram', $values['enable_share_instagram']);
    $config->set('sticky_social_bar.enable_share_stumbleupon', $values['enable_share_stumbleupon']);
    $config->set('sticky_social_bar.enable_whatsapp', $values['enable_whatsapp']);
    $config->set('sticky_social_bar.enable_share_reddit', $values['enable_share_reddit']);
    $config->set('sticky_social_bar.enable_share_fbook', $values['enable_share_fbook']);
    $config->set('sticky_social_bar.disable_email', $values['disable_email']);
    $config->set('sticky_social_bar.enable_bar', $values['enable_bar']);
    $config->set('sticky_social_bar.custom_color', $values['custom_color']);
    $config->set('sticky_social_bar.custom_color', $keycolor);
    $config->set('sticky_social_bar.node_types', $values['node_types']);

    $urltextbx = $form_state->getValue('urlchk');
    $urlchkbox = $form_state->getValue('urlupdate_check');

    foreach ($socialfrmsub as $socialkey => $media) {

      if ($form_state->hasValue($socialkey . '_api_url')) {
        $config->set('sticky_social_bar.' . $socialkey . '.api_url', $form_state->getValue($socialkey . '_api_url'));
      }
      if ($form_state->hasValue($socialkey . '_api_accesstoken_app_id')) {
        $config->set('sticky_social_bar.' . $socialkey . '.api_accesstoken_app_id', $form_state->getValue($socialkey . '_api_accesstoken_app_id'));
      }
      if ($form_state->hasValue($socialkey . '_api_accesstoken_app_secret')) {
        $config->set('sticky_social_bar.' . $socialkey . '.api_accesstoken_app_secret', $form_state->getValue($socialkey . '_api_accesstoken_app_secret'));
      }
      if ($form_state->hasValue($socialkey . '_api_accesstoken_pagename_app')) {
        $config->set('sticky_social_bar.' . $socialkey . '.api_accesstoken_pagename_app', $form_state->getValue($socialkey . '_api_accesstoken_pagename_app'));
      }
      $apiid .= $form_state->getValue($socialkey . '_api_accesstoken_app_id');
      $apisec .= $form_state->getValue($socialkey . '_api_accesstoken_app_secret');
      $apipagenm .= $form_state->getValue($socialkey . '_api_accesstoken_pagename_app');
    }

    $apisecendecry = encrypt_decrypt($apisec, 'encrypt');
    $apipagenmendecry = encrypt_decrypt($apipagenm, 'encrypt');
    $apiidendecry = encrypt_decrypt($apiid, 'encrypt');
    $fbstorageval = array(
      $apisecendecry,
      $apipagenmendecry,
      $apiidendecry
    );

    $tempstorefb = \Drupal::service('tempstore.private')->get('sticky_social_bar');
    $tempstorefb->set('fbstoragevalue', $fbstorageval);

    $urltextbxendecry = encrypt_decrypt($urltextbx, 'encrypt');
    $tempstoreurl = \Drupal::service('tempstore.private')->get('sticky_social_bar');
    $tempstoreurl->set('urltextbxval', $urltextbxendecry);

    $urlchkboxendecry = encrypt_decrypt($urlchkbox, 'encrypt');
    $tempstore = \Drupal::service('tempstore.private')->get('sticky_social_bar');
    $tempstore->set('my_variable_name', $urlchkboxendecry);

    $config->save();

    return parent::submitForm($form, $form_state);
  }



  /**
   * Get all available content entities in the environment.
   * @return array
   */
  public static function getContentEntities() {
    $content_entity_types = [];
    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions(); 
    foreach ($entity_type_definitions as $definition) {
      if ($definition instanceof ContentEntityType) {
        $content_entity_types[] = $definition;
      }
    }

    return $content_entity_types;
  }

  public function getBundles(array &$element, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $value = $triggeringElement['#value'];
    $bundlesObj = \Drupal::service('entity_type.bundle.info')->getBundleInfo($value);
    foreach ($bundlesObj as $key => $value) {
       $options[$key] = $value['label'] ;
    }
    $wrapper_id = $triggeringElement["#ajax"]["wrapper"];
    $renderedField = '';
    foreach ($options as $key => $value) {
      $renderedField .= "<option value='".$key."'>".$value."</option>";
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#".$wrapper_id, $renderedField));
    return $response;
  }


  /**
   * Get social media socialvariables.
   *
   * @return array
   *   Social media socialvariables.
   */
  function getSocialSBar() {
    $socialvariables = [
      'facebook_share' => 'Facebook share',
      'instagram'  => 'Instagram',
      'linkedin' => 'Linkedin',
      'twitter' => 'Twitter',
      'pinterest' => 'Pinterest',
      'whatsapp' => 'Whatsapp',
      'stumbleupon' => 'Stumbleupon',
      'reddit' => 'Reddit',
      'email' => 'Email',
    ];

    return $socialvariables;
  }



}
